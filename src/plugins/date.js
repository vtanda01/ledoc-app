// import dayjs from 'dayjs';

// export default {
//     install: (app) => {
//         app.config.globalProperties.$date = (dateString) => {
//             return dayjs(dateString).format('DD MMMM YYYY');
//         };
//     },
// };


// plugins/dayjs.js

import dayjs from 'dayjs';

export default {
    install: (app) => {
        app.config.globalProperties.$date = (dateString) => {
            return dayjs(dateString).format('DD/MM/YYYY');
        };
    },
};
