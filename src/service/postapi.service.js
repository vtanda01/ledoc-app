import axiosInstance from '@/config/axios.js';


export async function postPatients() {

    try {
      const response = await axiosInstance.post('/patients');
    //   localStorage.setItem('AUTH_TOKEN', response.data.jwt)
    //   console.log("les reponses", response.data.jwt)
      return response.data;

		} catch(error) {}
}


export async function postVisites() {

    try {
      const response = await axiosInstance.post('/visits');
      return response.data;

		} catch(error) {}
}


export async function postUpload() {

    try {
      const response = await axiosInstance.post('/upload');
      return response.data;

		} catch(error) {}
}


// export function isAuthenticated() {
//   const authToken = localStorage.getItem('AUTH_TOKEN');
//   return authToken && authToken !== '';
// }

// export function getToken() {
//    return localStorage.getItem('AUTH_TOKEN');
// }
// export function logout(){
//   localStorage.clear();
// }