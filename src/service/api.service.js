import axiosInstance from '@/config/axios.js';


export async function getPatients() {

    try {
      const response = await axiosInstance.get('/patients?populate=*');
      
      console.log( 'les patients : ', response.data)
      return response.data;

		} catch(error) {}
}


export async function getPatientId(patientid) {

  try {
    const response = await axiosInstance.get(`/patients/${patientid}?populate=*`);
    
    console.log( 'l\'id des patients: ', response.data)
    return response.data;

  } catch(error) {}
}


export async function getVisits() {

    try {
      const response = await axiosInstance.get('/visits');
      console.log( 'les visites : ', response.data)
      return response.data;

		} catch(error) {}
}

export async function getFiles() {

    try {
      const response = await axiosInstance.get('/upload/files');
      console.log( 'les fichiers : ', response.data)
      return response.data;

		} catch(error) {}
}

