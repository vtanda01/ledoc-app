import { ref } from 'vue';
import { useRouter } from 'vue-router';
import { getPatients } from '@/service/api.service';

const unPatient = ref ([]);
export default function UseApi() {
    const router = useRouter()
    
    const fetchPatients = async () => {
        try {
            
            unPatient.value = await getPatients();
                
            } catch (error) {
                console.error('Erreur lors de la récupération des identifiants :', error);
            }
        };
    
        
        return {
            fetchPatients,
            unPatient
        };
}
