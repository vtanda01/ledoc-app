import { ref } from 'vue';
import { useRouter } from 'vue-router';
import { getPatientId } from '@/service/api.service';

const patients = ref ([]);
export default function UsePatientId() {
 
    const fetchPatientsId = async (patientid) => {
        try {
            
            patients.value = await getPatientId(patientid);
                
            } catch (error) {
                console.error('Erreur lors de la récupération des identifiants du patient :', error);
            }
        };
        return {
            fetchPatientsId,
            patients
        };
}
