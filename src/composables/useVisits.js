import { ref } from 'vue';
import { useRouter } from 'vue-router';
import { getVisits } from '@/service/api.service';

export default function UseVisits() {
    const uneVisite = ref ([]);
    const router = useRouter()

        const fetchVisits = async () => {
            try {
            
                uneVisite.value = await getVisits();
                    console.log( 'les visites : ', getVisits)
                    
                } catch (error) {
                    console.error('Erreur lors de la récupération des visites :', error);
                }
            };

            return {
                fetchVisits,
                uneVisite
            };
}