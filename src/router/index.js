import { createRouter, createWebHistory } from 'vue-router'
import DashBoardView from '../views/DashBoardView.vue'
import PatientView from '../views/PatientView.vue'
import RdvView from '../views/RdvView.vue'
import NotFoundView from '../views/NotFoundView.vue'
import PatientView_details from '@/views/PatientView_details.vue'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'dashboard',
      component: DashBoardView
    },
    {
      path: '/patient',
      name: 'patient',
      component: PatientView
    },
    {
      path: '/rdv',
      name: 'rdv',
      component: RdvView
    },
    {
      path: '/detailsPatients/:id',
      name: 'detailsPatients',
      component: PatientView_details

      // path: '/detailsPatients',
      // name: 'detailsPatients',
      // component: PatientView_details,
      // props: true // Active les props dynamiques basées sur les paramètres d'URL
    },

    {
      path: '/:pathMatch(.*)*',
      name: 'notfound',
      component: NotFoundView
    },

  ]
})

export default router
