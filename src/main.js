import '@/assets/main.scss'
import 'bootstrap/dist/css/bootstrap.min.css';
import { createApp } from 'vue'
import { createPinia } from 'pinia'
import DatePlugin from '@/plugins/date';

import App from './App.vue'
import router from './router'

const app = createApp(App)
app.use(DatePlugin);
app.use(createPinia())
app.use(router)

app.mount('#app')
